#!/usr/bin/env bash
set -e 

AWS_DEV_ROLE=$1
APP_REGION=$2

aws sts assume-role --role-arn $AWS_DEV_ROLE --role-session-name rdp-develop-access > assume-role-output.txt
export AWS_ACCESS_KEY_ID=`cat assume-role-output.txt | jq -r '.Credentials.AccessKeyId'`
export AWS_SECRET_ACCESS_KEY=`cat assume-role-output.txt | jq -r '.Credentials.SecretAccessKey'`
export AWS_SESSION_TOKEN=`cat assume-role-output.txt | jq -r '.Credentials.SessionToken'`
export AWS_DEFAULT_REGION=$APP_REGION