#!/usr/bin/env bash

VSCODE_DIR=.vscode

if [ -d "$VSCODE_DIR" ]; then
  echo "ERR: $VSCODE_DIR has been setup. Remove it manually first if want to re-setup"
  return
fi

echo "setting up vscode"
mkdir .vscode
cp tools/artifacts/.env .vscode/.env
cp tools/artifacts/settings.json .vscode/settings.json
echo "..done"