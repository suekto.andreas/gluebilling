#!/usr/bin/env bash

PROJECT_NAME=$1
PROJECT_NAME_NO_WHITESPACE="$(echo -e "${PROJECT_NAME}" | tr -d '[:space:]')"

DOCS_DIR=docs

if [ -z "$PROJECT_NAME" ]; then 
    echo "ERR: PROJECT_NAME arg need to be provided"
    return
fi 

if [ -d "$DOCS_DIR" ]; then
    echo "ERR: $DOCS_DIR has been setup. Remove it manually first if want to re-setup"
    return
fi

echo "setting up documentation"
mkdir -p $DOCS_DIR
cp -R tools/artifacts/docs-template/* $DOCS_DIR
sed -i -e 's/--fill-in-project-name--/'"$PROJECT_NAME"'/g' $DOCS_DIR/source/conf.py
sed -i -e 's/--fill-in-project-name--/'"$PROJECT_NAME"'/g' $DOCS_DIR/source/index.rst

sed -i -e 's/--fill-in-project-html-basename--/'"$PROJECT_NAME_NO_WHITESPACE"'/g' $DOCS_DIR/source/conf.py
rm $DOCS_DIR/source/conf.py-e 
rm $DOCS_DIR/source/index.rst-e
echo "..done"