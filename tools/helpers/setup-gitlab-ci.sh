#!/usr/bin/env bash

if [ -f ".gitlab-ci.yml" ]; then
  echo "ERR: .gitlab-ci.yml has been setup. Remove it manually first if want to re-setup"
  return
fi

PROJECT_NAME=$1
PROJECT_NAME_NO_WHITESPACE="$(echo -e "${PROJECT_NAME}" | tr -d '[:space:]')"
if [ -z "$PROJECT_NAME" ]; then 
    echo "ERR: PROJECT_NAME arg need to be provided"
    return
fi 

PYTHON_SOURCE_CODE_PATH=$2
if [ -z "$PYTHON_SOURCE_CODE_PATH" ]; then 
    echo "ERR: PYTHON_SOURCE_CODE_PATH arg need to be provided"
    return
fi 

echo "setting up .gitlab-ci.yml"
cp -R tools/artifacts/.gitlab-ci.yml-template .gitlab-ci.yml
sed -i -e 's/--fill-in-project-name--/'"$PROJECT_NAME"'/g' .gitlab-ci.yml
sed -i -e 's/--fill-in-source-code-path--/'"$PYTHON_SOURCE_CODE_PATH"'/g' .gitlab-ci.yml

rm .gitlab-ci.yml-e
echo "..done"