#!/usr/bin/env bash

set -e 

SOURCE_CODE_PATH=$1

if [ ! -d "venv" ]; then
    echo "ERR: virtualenv need to be setup first. Run init.sh"
    return
fi
. venv/bin/activate
pip freeze > requirements.txt