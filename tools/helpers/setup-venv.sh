#!/usr/bin/env bash

# Basic VENV
VENV_DIR=venv

if [ -d "$VENV_DIR" ]; then
  echo "ERR: $VENV_DIR has been setup. Remove it manually first if want to re-setup"
  return
fi

echo "setting up virtualenv"
virtualenv venv 
. venv/bin/activate
pip install autopep8
pip install pylint
pip install coverage
pip install sphinx
pip install sphinx_rtd_theme
echo "..done"

# PySpark AWS Glue Specific
echo "setting up AWS Glue PySpark dependencies"
pip install pyspark
AWSGLUE_DIR=$VENV_DIR/lib/python2.7/site-packages/awsglue

if [ -d "$AWSGLUE_DIR" ]; then
  echo "ERR: $AWSGLUE_DIR has been setup. Remove it manually first if want to re-setup"
  return
fi

mkdir -p .tmp
cd .tmp
curl -O https://s3.amazonaws.com/aws-glue-jes-prod-us-east-1-assets/etl/python/PyGlue.zip
unzip PyGlue.zip
cp -R awsglue "../"$AWSGLUE_DIR
cd ..
rm -Rf .tmp/PyGlue.zip
echo "..done" 