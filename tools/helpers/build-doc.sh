#!/usr/bin/env bash


SOURCE_CODE_PATH=$1
DOCS_DIR=docs

if [ -z "$SOURCE_CODE_PATH" ]; then 
    read -p "Enter SOURCE_CODE_PATH: " SOURCE_CODE_PATH
    if [ -z "$SOURCE_CODE_PATH" ]; then 
        echo "ERR: SOURCE_CODE_PATH arg need to be provided"
        return
    fi
fi

if [ ! -d "$DOCS_DIR" ]; then
    echo "ERR: $DOCS_DIR need to be setup first. Run init.sh"
    return
fi

. venv/bin/activate
rm -Rf $DOCS_DIR/build
rm -Rf $DOCS_DIR/source/docstring
sphinx-apidoc -f -o $DOCS_DIR/source/docstring/ $SOURCE_CODE_PATH
cd $DOCS_DIR && make html && cd ..
