#!/usr/bin/env bash

set -e 

if [ -z "$PROJECT_NAME" ]; then
    PROJECT_NAME=$1
    if [ -z "$PROJECT_NAME" ]; then
        read -p "Enter Project Name: " PROJECT_NAME
    fi

    if [ -z "$PROJECT_NAME" ]; then
        echo "ERR: Project Name need to be provided"
        return
    fi
fi 

if [ -z "$PYTHON_SOURCE_CODE_DIR" ]; then
    PYTHON_SOURCE_CODE_DIR=$2
    if [ -z "$PYTHON_SOURCE_CODE_DIR" ]; then
        read -p "Enter Python Source Code Dir Path: " PYTHON_SOURCE_CODE_DIR
    fi

    if [ -z "$PYTHON_SOURCE_CODE_DIR" ]; then
        echo "ERR: Python Source Code Dir Path need to be provided"
        return
    fi
fi

# Set Virtualenv for Development
. tools/helpers/setup-venv.sh

# Set VSCode Workspace
. tools/helpers/setup-vscode.sh

# Set Documentation
mkdir -p .tmp
. tools/helpers/setup-doc.sh "$PROJECT_NAME"

# Set gitlab CI
. tools/helpers/setup-gitlab-ci.sh "$PROJECT_NAME" $PYTHON_SOURCE_CODE_DIR

# Check requirements.txt
if [ -f "requirements.txt" ]; then
    . venv/bin/activate
    pip install -r requirements.txt
fi