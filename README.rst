.. image:: https://gitlab.com/suekto.andreas/gluebilling/badges/master/pipeline.svg
   :target: https://gitlab.com/suekto.andreas/gluebilling/commits/master

.. image:: https://gitlab.com/suekto.andreas/gluebilling/badges/master/coverage.svg
   :target: https://gitlab.com/suekto.andreas/gluebilling/commits/master

Glue Billing
=======
Simplified sample Project Calculating AWS Glue ETL Billing Daily Summary.
With the goal of showing a sample code of how to do unittesting in PySpark.

Dependency
------------
1. python 2.7
2. `pip install vitualenv`
3. Visual Studio Code 
4. VSCode Plugin : https://github.com/ryanluker/vscode-coverage-gutters
5. VSCode Plugin : https://github.com/emeraldwalk/vscode-runonsave
6. Bash Script
7. JDK 1.8 with JAVA_HOME set properly

Installation
-------------------
Run the following Bash script
.. code-block::

    . tools/init.sh "Glue Billing" gluebilling

Build Project
-------------
To Build the Project run the following command
.. code-block::

    . tools/build.sh gluebilling


Effects

* build documentation source scripts ``docs/``
* pip freeze ``requirements.txt``

