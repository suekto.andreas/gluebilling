"""Process records of ETL Job usage into the billing of fee"""
from datetime import datetime as dt
from decimal import Decimal
from pyspark.sql.types import StructField, StructType
from pyspark.sql.types import IntegerType, StringType
from pyspark.sql.types import DecimalType
from pyspark.sql.functions import udf


def calculate_duration(from_timestamp, to_timestamp):
    """Returns duration in second between two
    time provided in unix timestamp"""
    from_dt = dt.fromtimestamp(float(from_timestamp))
    to_dt = dt.fromtimestamp(float(to_timestamp))

    time_delta = to_dt - from_dt
    return int(time_delta.total_seconds())


def calculate_fee(duration_in_second, dpu_num,
                  minimum_duration, fee_dpu_hour):
    """Returns a decimal of the fee incurred in USD,
    quantized into 8 digit behind comma"""

    charged_duration = duration_in_second
    if charged_duration < minimum_duration:
        charged_duration = minimum_duration

    fee = (charged_duration * dpu_num * Decimal(fee_dpu_hour)) / 3600
    return fee.quantize(Decimal('0.00000000'))


def get_usage_record_schema():
    """Retruns StructType containing the Input Usage
    Data Expected Schema"""
    return StructType([
        StructField("job_id", StringType(), False),
        StructField("type", StringType(), False),
        StructField("dpu", IntegerType(), False),
        StructField("from_unix_timestamp", StringType(), False),
        StructField("to_unix_timestamp", StringType(), False)
    ])


def get_pricing_schema():
    """Retruns StructType containing the Input
     Pricing Data Expected Schema"""
    return StructType([
        StructField("type", StringType(), False),
        StructField("dpu_hour", StringType(), False),
        StructField("minimum_duration", IntegerType(), False)
    ])


def get_billing_schema():
    """Retruns StructType containing the Billing Schema"""
    return StructType([
        StructField("job_id", StringType(), False),
        StructField("type", StringType(), False),
        StructField("dpu", IntegerType(), False),
        StructField("from_unix_timestamp", StringType(), False),
        StructField("to_unix_timestamp", StringType(), False),
        StructField("dpu_hour", StringType(), False),
        StructField("minimum_duration", IntegerType(), False),
        StructField("duration", IntegerType(), False),
        StructField("fee", DecimalType(20, 8), False),
    ])


def generate_billing(usage_df, pricing_df):
    """Returns DataFrame of Fee from a DataFrame of Usage Records"""

    duration_udf = udf(calculate_duration, IntegerType())

    join_data_df = usage_df.join(
        pricing_df,
        usage_df.type == pricing_df.type
    ).select(
        usage_df.job_id, usage_df.type,
        usage_df.dpu, usage_df.from_unix_timestamp,
        usage_df.to_unix_timestamp,
        pricing_df.dpu_hour, pricing_df.minimum_duration,
        duration_udf(
            usage_df.from_unix_timestamp,
            usage_df.to_unix_timestamp).alias("duration")
    )

    fee_udf = udf(calculate_fee, DecimalType(20, 8))

    billing_df = join_data_df.select(
        "job_id", "type", "dpu", "from_unix_timestamp",
        "to_unix_timestamp", "dpu_hour", "minimum_duration", "duration",
        fee_udf(
            join_data_df.duration, join_data_df.dpu,
            join_data_df.minimum_duration, join_data_df.dpu_hour
        ).alias("fee")
    )

    return billing_df
