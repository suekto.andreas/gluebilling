"""Unit Testing for Glue Billing Project"""
import unittest
from decimal import Decimal

import gluebilling.billing as billing


class CalculateDurationTest(unittest.TestCase):
    """Test Cases for Glue Billing Unit Testing"""

    def test_positive_duration(self):
        """Test successfully calculate duration
        between 2 unix timestamp string"""
        duration = billing.calculate_duration("1535824800", "1535835600")
        self.assertEqual(duration, 10800)

    def test_negative_duration(self):
        """Test successfully generate negative number"""
        duration = billing.calculate_duration("1535835600", "1535824800")
        self.assertEqual(duration, -10800)


class CalculateFeeTest(unittest.TestCase):
    """Unit Test for calculate_fee function"""

    def test_more_than_10_min(self):
        """When usage is more than 10 min"""
        fee = billing.calculate_fee(800, 3, 600, 0.44)
        self.assertEqual(fee, Decimal("0.29333333"))
