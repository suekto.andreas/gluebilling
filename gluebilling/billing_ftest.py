"""Functional Testing for Glue Billing Project"""
from decimal import Decimal
from pyspark.sql import Row, SQLContext

import gluebilling.billing as billing
import gluebilling.pyspark_htest as pysparktest


class GenerateBillingTest(pysparktest.PySparkTest):
    """Test Cases for Generate Billing"""

    def generate_usage_data_001(self):
        """Generate usage data for testing it is a record of
        AWS Glue ETL usage"""
        rdd = self.spark.sparkContext.parallelize([
            Row("JOB001", "etl", 3, "1535824800", "1535835600"),
            Row("JOB002", "crawler", 3, "1535824800", "1535824850"),
            Row("JOB003", "crawler", 3, "1535824800", "1535835600")
        ])
        schema = billing.get_usage_record_schema()

        sqlctx = SQLContext(self.spark.sparkContext)
        return sqlctx.createDataFrame(rdd, schema)

    def generate_pricing_data_001(self):
        """Generate pricing data for testing it is a record of
        AWS Glue ETL usage"""
        rdd = self.spark.sparkContext.parallelize([
            Row("etl", "0.44", 600),
            Row("crawler", "0.20", 200)
        ])
        schema = billing.get_pricing_schema()

        sqlctx = SQLContext(self.spark.sparkContext)
        return sqlctx.createDataFrame(rdd, schema)

    def generate_expected_billing_001(self):
        """Generate expected billing"""
        rdd = self.spark.sparkContext.parallelize([
            Row("JOB001", "etl", 3, "1535824800", "1535835600", "0.44",
                600, 10800, Decimal("3.96")),
            Row("JOB002", "crawler", 3, "1535824800", "1535824850", "0.20",
                200, 50, Decimal("0.03333333")),
            Row("JOB003", "crawler", 3, "1535824800", "1535835600", "0.20",
                200, 10800, Decimal("1.80"))
        ])
        schema = billing.get_billing_schema()

        sqlctx = SQLContext(self.spark.sparkContext)
        return sqlctx.createDataFrame(rdd, schema)

    def test_with_set_001(self):
        """Using all 001 test data set"""
        usage_df = self.generate_usage_data_001()
        pricing_df = self.generate_pricing_data_001()
        expected = self.generate_expected_billing_001()

        actual = billing.generate_billing(usage_df, pricing_df)

        self.assert_dataframe_equal(actual, expected, ["job_id"])
